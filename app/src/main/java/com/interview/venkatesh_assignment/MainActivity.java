package com.interview.venkatesh_assignment;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.interview.venkatesh_assignment.Adapter.MovieListAdapter;

import java.util.List;

import model.MovieListViewModel;
import model.Search;
import model.ViewModelFactory;

public class MainActivity extends AppCompatActivity {

    MovieListViewModel movieListViewModel;
    private MovieListAdapter adapter;
    private RecyclerView recycle_movie_list;
    private TextView NoDataFound;

    public static boolean checkNetworkStatus(final Context context) {
        if (context != null) {
            try {
                ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                final NetworkInfo activeNetwork = connectivity.getActiveNetworkInfo();
                final boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
                if (!isConnected) {

                }
                return isConnected;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        NoDataFound = findViewById(R.id.txtNoData);
        recycle_movie_list = findViewById(R.id.recycle_movie_list);
        movieListViewModel = new ViewModelProvider(this,
                new ViewModelFactory(getApplication(), 1))
                .get(MovieListViewModel.class);
        adapter = new MovieListAdapter(this);
        recycle_movie_list.setAdapter(adapter);
        SearchView search = findViewById(R.id.viewAllSearchView);
        search.findViewById(R.id.search_close_btn)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

                        search.setQuery("", false);
                        adapter.submitList(null);
                        loadMovieList("Batman");
                    }
                });
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                adapter.submitList(null);
                loadMovieList(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });
        ObserveLiveData();
        loadMovieList("Batman");
    }

    private void ObserveLiveData() {
        movieListViewModel.getMovieList().observe(this, new Observer<List<Search>>() {
            @Override
            public void onChanged(List<Search> searchList) {
                if (searchList != null && searchList.size() > 0) {
                    NoDataFound.setVisibility(View.GONE);
                    recycle_movie_list.setVisibility(View.VISIBLE);
                    adapter.submitList(searchList);
                } else {
                    NoDataFound.setVisibility(View.VISIBLE);
                    recycle_movie_list.setVisibility(View.GONE);
                }
            }

        });
    }

    public void loadMovieList(String query) {
        if (checkNetworkStatus(this)) {
            movieListViewModel.getMovieList(query);
        }
    }
}