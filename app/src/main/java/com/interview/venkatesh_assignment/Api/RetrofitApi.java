package com.interview.venkatesh_assignment.Api;

import model.MovieList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitApi {

    @GET(".")
    Call<MovieList> getMovieList(@Query("s") String search_str, @Query("apikey") String api_key);
}
