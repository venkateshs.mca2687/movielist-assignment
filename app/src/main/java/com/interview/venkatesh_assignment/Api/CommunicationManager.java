package com.interview.venkatesh_assignment.Api;

import android.util.Log;

import java.util.concurrent.TimeUnit;

import model.MovieList;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CommunicationManager {
    private static CommunicationManager mCommunicationManager;

    /**
     * method to access the API singleton.
     *
     * @return instance instance
     */
    public static CommunicationManager getInstance() {
        if (mCommunicationManager == null) {
            mCommunicationManager = new CommunicationManager();
        }
        return mCommunicationManager;
    }

    public RetrofitApi getInstanceRetrofit() {
        RetrofitApi retrofitApi = null;
        final String contentType = "application/json";
        String url = "https://www.omdbapi.com/";
        try {
            if (url != null) {
                OkHttpClient okHttpClient;
                HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
                loggingInterceptor.level(HttpLoggingInterceptor.Level.BODY);

                okHttpClient = new OkHttpClient.Builder()
                        .readTimeout(1000, TimeUnit.SECONDS)
                        .connectTimeout(1000, TimeUnit.SECONDS)
                        .addInterceptor(loggingInterceptor)
                        .build();

                Log.d("tutor", "getInstanceRetrofit: " + url);
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(url)
                        .client(okHttpClient)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                retrofitApi = retrofit.create(RetrofitApi.class);
            }
        } catch (Exception e) {

        }

        return retrofitApi;
    }

    public Call<MovieList> getMovieList(String key) {
        try {

            return getInstanceRetrofit()
                    .getMovieList(key, "780b4991");
        } catch (Exception e) {
            return null;
        }
    }
}
