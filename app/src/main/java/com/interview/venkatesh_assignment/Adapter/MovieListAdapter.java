package com.interview.venkatesh_assignment.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.interview.venkatesh_assignment.R;

import model.Search;


public class MovieListAdapter extends ListAdapter<Search, MovieListAdapter.MovieViewHolder> {

    Context scrn_context;

    public MovieListAdapter(Context context) {
        super(Search.DIFF_CALLBACK);
        scrn_context = context;
    }


    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MovieViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_movie_list, parent, false));
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        Search searchObj = getItem(position);
        holder.txt_movie_name.setText(searchObj.getTitle());
        holder.txt_year.setText(searchObj.getYear());
        Glide.with(scrn_context)
                .load(searchObj.getPoster())
                .into(holder.img_movie);
    }


    public class MovieViewHolder extends RecyclerView.ViewHolder {
        TextView txt_movie_name, txt_year;
        ImageView img_movie;

        public MovieViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_year = itemView.findViewById(R.id.txt_year);
            txt_movie_name = itemView.findViewById(R.id.txt_movie_name);
            img_movie = itemView.findViewById(R.id.img_movie);
        }
    }
}
