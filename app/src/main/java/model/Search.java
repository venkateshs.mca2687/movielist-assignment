
package model;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Search {

    public static DiffUtil.ItemCallback<Search> DIFF_CALLBACK = new DiffUtil.ItemCallback<Search>() {
        @Override
        public boolean areItemsTheSame(@NonNull Search oldItem, @NonNull Search newItem) {
            return oldItem.getImdbID().equals(newItem.getImdbID());
        }

        @Override
        public boolean areContentsTheSame(@NonNull Search oldItem, @NonNull Search newItem) {
            return oldItem.equals(newItem);
        }
    };
    @SerializedName("Title")
    @Expose
    public String title;
    @SerializedName("Year")
    @Expose
    public String year;
    @SerializedName("imdbID")
    @Expose
    public String imdbID;
    @SerializedName("Type")
    @Expose
    public String type;
    @SerializedName("Poster")
    @Expose
    public String poster;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Search car = (Search) o;
        return Objects.equals(imdbID, car.getImdbID());
    }

    @Override
    public int hashCode() {
        return Objects.hash(imdbID);
    }

    public String getTitle() {
        return title;
    }

    public String getYear() {
        return year;
    }

    public String getImdbID() {
        return imdbID;
    }

    public String getType() {
        return type;
    }

    public String getPoster() {
        return poster;
    }
}
