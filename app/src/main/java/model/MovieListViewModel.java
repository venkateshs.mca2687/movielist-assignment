package model;


import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.interview.venkatesh_assignment.Api.CommunicationManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieListViewModel extends AndroidViewModel {
    private long id;
    private MutableLiveData<List<Search>> search_movie_list = new MutableLiveData<>();
    //    private List<EducatorNotificationDataModel> notificationListData;
    private MutableLiveData<Boolean> isStop = new MutableLiveData<>();

    public MovieListViewModel(@NonNull Application application, final long id) {
        super(application);
        this.id = id;
    }

    public void getMovieList(String key) {

        load_movie_list(0, key);

    }

    public LiveData<Boolean> isStopped() {
        return isStop;
    }

    public LiveData<List<Search>> getMovieList() {
        return search_movie_list;
    }

    private void load_movie_list(int page, String Key) {
        CommunicationManager communicationManager = CommunicationManager.getInstance();
        Call<MovieList> call = communicationManager.getMovieList(Key);
        if (call != null) {
            call.enqueue(new Callback<MovieList>() {
                @Override
                public void onResponse(Call<MovieList> call, Response<MovieList> response) {
                    MovieList baseResponseObj = response.body();
                    if (baseResponseObj != null && baseResponseObj.getSearch() != null && baseResponseObj.getSearch().size() > 0) {

//                            notificationListData.addAll(educatorNotificationDataModelList);
//                        }
                        if (page != 0) {
                            if (search_movie_list != null && search_movie_list.getValue() != null && search_movie_list.getValue().size() > 0)
                                baseResponseObj.getSearch().addAll(search_movie_list.getValue());
                        }
                        search_movie_list.setValue(baseResponseObj.getSearch());
                    } else {
                        search_movie_list.setValue(null);
                    }
                }

                @Override
                public void onFailure(Call<MovieList> call, Throwable t) {
                    search_movie_list.setValue(null);
                }
            });
        }
    }
}
